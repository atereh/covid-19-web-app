import { Component, OnInit } from '@angular/core';
import { News } from '../models/news.model';
import { Signinservice } from '../signin.service';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  title: any;
  description: any;

  constructor(private signInService : Signinservice) { }

  ngOnInit(): void {
  }

  addNews(){
    let news: News = {
      title: this.title,
      description: this.description
    };

    this.signInService.addNews(news);
    this.title = undefined;
    this.description = undefined;

  }

}
