import { Component, OnInit } from '@angular/core';
import { Awaitable } from '../models/awaitable.model';
import { AggregatedCountryDaySummary, Country, CountrySummary } from '../models/covid-api';
import { StatsService } from '../statistics.service';
import { ActivatedRoute } from '@angular/router';
import { LoadService } from '../load.service';
import startOfToday from 'date-fns/startOfToday';
import subDays from 'date-fns/subDays';


@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  private countryEntireHistory: Awaitable<AggregatedCountryDaySummary[]>;
  private countrySummary: Awaitable<CountrySummary>;
  
  constructor(
    private readonly statsService: StatsService,
    private readonly loadService: LoadService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(async (paramMap) => {
      const countrySlug = paramMap.get('countrySlug');
      await this.tryUpdateSummaries(countrySlug);
    });
  }


  private async tryUpdateSummaries(countrySlug: string): Promise<void> {
    try {
      this.countryEntireHistory = { state: 'loading' };
      if (countrySlug === null) {
        throw new Error('CountrySlug was not provided');
      }
      const summary = await this.statsService.getEntireHistoryForCountry(
        countrySlug
      );
      this.countryEntireHistory = {
        state: 'success',
        data: summary,
        lastFetched: new Date(),
      };
    } catch (error) {
      this.countryEntireHistory = { state: 'error', error };
    }
  }

  public get summary(): CountrySummary {
    if (this.loadService.isSuccess(this.countrySummary)) {
      return this.countrySummary.data;
    } else {
      throw new Error('Summary not fetched');
    }
  }


  public isLoading(): boolean {
    return this.loadService.isLoading(this.countryEntireHistory);
  }

  public getEntireHistory(): AggregatedCountryDaySummary[] {
    if (this.loadService.isSuccess(this.countryEntireHistory)) {
      return this.countryEntireHistory.data;
    } else {
      return [];
    }
  }

  public getLastSevenDays(): AggregatedCountryDaySummary[] {
    const history = this.getEntireHistory();
    const today = startOfToday();
    const weekAgo = subDays(today, 7);
    return history.filter((x) => new Date(x.Date) >= weekAgo);
  }
}