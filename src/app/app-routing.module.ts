import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { CountriesComponent } from './countries/countries.component';
import { CountryComponent } from './country/country.component';
import { NewsComponent } from './news/news.component';
import { SecurePagesGuard } from './secure-pages.guard';
import { SigninComponent } from './signin/signin.component';

const routes: Routes = [

  { path: 'countries', component: CountriesComponent },
  { path: 'countries/:countrySlug', component: CountryComponent },
  { path: 'signin', component: SigninComponent, canActivate : [SecurePagesGuard]},
  { path: 'news', component: NewsComponent, canActivate : [AuthGuard]},
  { path: '', redirectTo: 'countries', pathMatch: 'full' },
  { path: '**', redirectTo: 'countries'}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
