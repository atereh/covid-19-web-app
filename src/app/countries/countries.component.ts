//https://api.covid19api.com/summary

import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet } from 'ng2-charts';
import { LoadService } from '../load.service';
import { Awaitable } from '../models/awaitable.model';
import { AllCountriesSummary, CountrySummary, GlobalSummary } from '../models/covid-api';
import { statPie } from '../models/statistics.model';
import { StatsService } from '../statistics.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css'],
})

export class CountriesComponent implements OnInit {
  public countriesSummary: Awaitable<AllCountriesSummary>;


  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  pieChartLabels: Label[] = ['Dead Cases', 'Recovered Cases', 'Active Cases'];
  pieChartData: SingleDataSet = [2171423, 55462903, 100672954-55462903-2171423]; //hard coded values here
  //I got completely lost with all these web things
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor(
    private readonly statsService: StatsService,
    public readonly loadService: LoadService
  ) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  public async ngOnInit(): Promise<void> {
    try {
      this.countriesSummary = { state: 'loading' };
      const summary = await this.statsService.getWorldWideSummary();
      this.countriesSummary = {
        state: 'success',
        data: summary,
        lastFetched: new Date(),
      };
    } catch (error) {
      this.countriesSummary = { state: 'error', error };
    }
  }

  public getCountries(): CountrySummary[] {
    if (this.loadService.isSuccess(this.countriesSummary)) {
      return this.countriesSummary.data.Countries;
    } else {
      return [];
    }
  }

  public getError(): string | null {
    if (this.loadService.isError(this.countriesSummary)) {
      return this.countriesSummary.error.message;
    } else {
      return null;
    }
  }

}