export interface User {
    uid: string | undefined;
    displayName: string | null | undefined;
    email: string | null | undefined;
}