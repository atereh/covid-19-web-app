export interface statPie {
    totalConfirmed: number;
    totalRecovered: number;
    totalDeaths: number;
  }