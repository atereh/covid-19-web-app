import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Signinservice } from './signin.service';

@Injectable({
  providedIn: 'root'
})
export class SecurePagesGuard implements CanActivate {
  constructor(private signInService: Signinservice, private router : Router){};
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     //check if user can access a certain layout
     if(this.signInService.userSignedIn()){ //check if user signed in
       
      this.router.navigate(["news"]); //navigate to news page
      }
    return true;
  }
  
}
