import { Component, OnInit } from '@angular/core';
import { News } from '../models/news.model';
import { User } from '../models/user.model';
import { Signinservice } from '../signin.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  user : User;
  news : News[];

  constructor(public signInService: Signinservice) { }

  ngOnInit(): void {
    this.user = this.signInService.getUser();
    this.signInService.getNews()
    .subscribe((news: News[]) => {
      this.news = news;
    });
  }

}
